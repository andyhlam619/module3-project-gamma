#Zeke's Journal

## March 8, 2023

* Available Styling

Today I was able to learn a new motion libray and add it to my available swoops
listing page. It was extremly helpful and made our project a little bit more inter
active when the motion finally worked.

## March 6, 2023

* Deployment test

This was our pit of dispair, we had alloated 2 days, 1 to fix our CI/CD pipelines and
the other day to get our backend and frontend deployed. This was extremely hard
and sometimes frustrating because in order for us to really test our deployment,
we had to push and wait for the builds and wait for our deployed site to be up.
We got assistance from our SEIRS and finally, we were able to get our deployed site
actually working. Now came the easy part, which was fixing up our endpoints and
replacing the host URLs everywhere so that it can properly run.

## March 2, 2023

* Mainpage Signed Out

Today we talked about how we should have a landing page for our frontend application
because it would feel 'weird' if we just went straight to a login page for our
website. We decided to design this and I took charge of coding and designing so that
it gave our application a bit of personality. My main goal here was to add a card
of a carousel images, so that it felt interactive when the user came to our landing
page. I was stuck on this for awhile not knowing how to put my card that had
some nav links in front of the carousel images. I finally figured it out by wrapping
the carousel with my card component and it worked!

## February 28, 2023

* Available Swoops - Frontend

Started the frontend portion today for the available swoops list. This was a bit hard
switching back to a different language, and I definitely had to refresh up back on
react and JSX. It took a bit of time to get used to hooks again, state, etc., but
once I started to code and get the same errors I've seen before, I was able to just
code with ease!

## February 22, 2023

* Assign swooper ID to post

This was stumping me for awhile, as we had to re-talk about how we want our data
to act. To try to simplify things, we decided to keep one user's profile, and use
that same profile but update hidden / optional fields so that they are now considered
a 'swooper' or a worker. What made me confused was that now instead of having a
completely new swooper ID, their swooper ID is actually their original customer ID
but we are assigning the swooper ID column based on that.

Once I realized that they are the same and we just need to make sure our SQL is joining
in a different way, we were able to update perfectly and smoothly in our backend.

## February 17, 2023

* Swooper accepts a job post

I worked on this feature, which could be said that it was another feature within
a feature. After being able to see the whole available listings from customers, I
had to really think about how I wanted to change the data so that we can upadte the
post so it has the necessary information, such as the swooper's id, updating the
status, and grabbing the posts details.

## February 16, 2023

* Created list view for Available Swoops and in our query and router folder

This was a bit challenging, as we FastAPI was fairly new to me. I had to read the
docs, and really understand how SQL likes to be formatted to successfully create
our tables and manipulate the data in our database. I was stuck on what our queries
folder basemodel was doing for us, but realized it was helping us know what data we
should expect to be returned out

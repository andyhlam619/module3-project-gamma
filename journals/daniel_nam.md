###3/8/23 - 3/9/23
- Spent a lot of time adding tiny QOL improvements to styling. Looks amazing and learned a lot in terms of css and how to design a great looking website

###3/3/23 - 3/7/23
- Completed Deployment
- Still need to figure out minor fixes with deployment but it overall works
- Got a lot of progress on stlyin, I completed the main hero section on the logged-in maing page

###2/28/23 - 3/2/23
- Mostly worked on styling on my components.
- Completed unit testing

###2/27/23
- Completed all of my front end components
- Got SwoopHistoryList component to be dynamic and show 2 components in one page

###2/24/23
- Continued to work on front end component.
- Got Swooper History List to work. Data shows the way I want it to.

###2/23/23
- Worked together to figure out front end auth.
- Attempted RTK query with auth but was advised not to by instructors so wasted a lot of time
- Finished Login with auth and attempted sign up but there is a 401 error

###2/22/23
- Went through entire test of backend using swagger and made fixes to our different endpoints. As of right now, the backend is completed.

###2/21/23
- Updated backend to utilize the token that was generated from user authentication.

###2/16/23
- Finished my portion of the backend
- Helped complete Auth

###2/15/23
- Helped create tables for user/swoops migration
- Pushed changes to docker-compose.yaml and had team help edit changes
- Merge request to main to main
- Inserted dummy data into pg-admin

###2/14/23
- Got docker.yaml file to work
- Established connection to pg-admin
- tested dummy data in pg-admin to make sure connection was established

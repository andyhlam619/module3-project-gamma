#Andy's Diary

###3/9/2023
Dear Diary,
- We did more styling and some finishing touches to our website for turning it in.
- We tested our entire application on the deployed app.
- Everything works.
- Worked on more fun styling.

###3/8/2023
Dear Diary,
- Worked on Styling
- Completed our ReadMe
- Pipeline minutes ran out minutes. Stopped all our pushes and waited until the next day.

###3/7/2023
Dear Diary,
- Completed deployment (need to work out some kinks with the domain url path but this is a very minor problem)
- More styling
- Closing out issues and attaching it to the right merge request
- We started Readme

###3/6/2023
Dear Diary,
- Deployed Backend and Frontend successfully
- Fixed CORS errors and yaml files
- Fully tested deployment again
- More styling
- Potentially start on websockets/chat feature
- Added a profile page with update and delete functions

###3/2/2023
Dear Diary,
- Completed Unit Tests
- Worked and corrected our CI/CD pipelines
- Once completed, will work on more styling across our app

###3/1/2023
Dear Diary,
- We were continuing to work on our styling
- Started CI/CD and Unit testing. Had a lot of trouble with this.
- Looked into Websockets for chat feature

###2/28/2023
Dear Diary,
- Worked on token / authorization working properly across all components
- Styling of our app

###2/27/2023
Dear Diary,
- We finished up the entire front end. I worked on adding a navbar.
- Merge all our components and test functionality for entire app of frontend
- Everything seemed to work

###2/24/2023
Dear Diary,
- Got Frontend Auth working with token for Login Form
- Got Frontend Auth working with token for Sign Up Form
- Ensured tokens are working properly
- Start working on React components for the pick up form

###2/23/2023
Dear Diary,
- Worked on front end and try to implement redux
- Do peer programming and split off into customer group, swooper group, and user group.
- I worked with Anthony.
- Ask instructors how to implement unit tests
- Later that day we found out redux was too difficult so we went back to how we did react. We will combat that at a later time AFTER we get it all working.

###2/22/2023
Dear Diary,
- We do a merge party. Everyone pushes and pulls. So we all have good working main branch each time.
- Tested to make sure all of the endpoints work
- need to ensure swooper form sign up works correctly
- ensure swooper IDs are being assigned correctly for accept
- ensure swooper IDs are being signed correctly for complete
- ensure customer IDs are being assigned properly for both accept method and complete method
- Start planning/working on front end

###2/21/2023
Dear Diary,
- Completed end point for my pick up form
- Updated endpoints to include current user data
- Needed to fix user table for Phone data type to be something other than just int
- Changed to str

###2/16/2023
Dear Diary,
- Today we planned out each feature that we are going to work on
- Split up into pairs and work on each feature
- Try to finish all endpoints

###2/15/2023
Dear Diary,
- We worked on our issue board to make sure we are all on the same page.
- We worked on fixing the docker-compose.yaml file.
- Checked to see if our table migrations worked. It did.
- Then we wanted to check if what we wrote so far worked. This is where we ran into an error. Some of our services were not running in the docker containers.
- We found out is was the DATABASE_URL in the yaml file. We had to fix that.
- We did and now the yaml file worked and the rest of our application works. We verified that on pg-admin our tables showed.
- Now we want to work on the end points.

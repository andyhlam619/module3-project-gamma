#Anthony's Journal

March 9, 2023

Today, I added to stylizing of my list view to make it consistent with the rest of our site. The biggest “aha” moment for me is utilizing the various libraries that we can find out there. Aside from Bootstrap, which is an amazing library, we discovered some other libraries out there that really upped our production value for our website. I finalized the detail view as well with the help of Daniel. It was really cool to finally get over the hump of the wall that I faced with front-end dev

March 8, 2023

Today, I focused on stylizing my components. After a day off from styling, I figured I’d give it a try. I was able to successfully implement a style to my list view. This wasn’t as bad as I thought, it took me a minute to understand how it all works together with “Col” and “Row” in regards to the format that I had with tables and rows. Once I figured that out, it made a ton of sense and I felt much better with front-end dev.

March 7, 2023

Today, I took on the responsibility of working on the ReadMe documentation which outlines our entire application and what it’s designed the way that it is. I had extensive practice with this during Mod2 and felt like I had a good understanding of how to effectively communicate this in the readme. I also took on this because I was hitting a wall during the stylization of the component so I wanted to revisit that later. The biggest “aha” for me was that the readme was actually a great refresher on the concepts we’ve learned up until this point. It was advantageous to recall pydantic models, and the relationships between queries and routers and [main.py](http://main.py) file. I really enjoyed working on this.

March 6, 2023

Today, we completed CI/CD. We kept getting a CORS error because something in our code was broken. We had to go through and make sure the paths were correct and continue to check our linting error before we pushed to the main branch. We spent some time going over the application to make sure it fully works. I spent a majority of the day on styling and my biggest “aha” moment was that I need to focus on the front-end development skills. I struggle with creatively putting a website together and am consistently inspired by the ideas my classmates are thinking about implementing. I’ve been able to see some brainstorming happen by referencing websites such as Uber or other applications and finding a way to implement that into our website.

March 3, 2023

Today, we continued working on the CI/CD portion of our application. We all tackled this as a team as this is something we all wanted to figure out. Zeke shared his screen as we all followed along. Halfway through the day, we were supplied the 3rd part of the deployment. We followed the steps but still were running into issues. Tracey, the SEIR, helped us traceback our steps and we noticed that our naming convention was off. That was the biggest aha moment for me. When we cloned the project, the public url or sample-service api in our dockerfile was named as default but when we started our project, we called our project Swoop-Service not Sample-Service. This solved a major part of the issue. We also went back and changed out anywhere we had hard-coded “localhost:8080” in our url’s and updated it to be more dynamic with the correct variable.

March 2, 2023

Today, we spend the entire day dedicated to CI/CD. We wanted to get a heads start on this but realized we had been doing it wrong the entire time. The biggest “aha” moment for me was that we needed to have our pipeline succeed every time we push to gitlab. This can be done by making sure that everyone puts their card information on gitlab to verify that a person is behind the account. And then from there, we need to make sure we fix the linting error and to make sure that flake8 was running. Prior to this, we just pushed and moved on. When we figured out what we needed to do, we jumped on live share on VSCode and edited Zeke’s screen so that we can avoid any merge conflicts. We made sure we indented everything correctly, if a line was too long, we would practice narrow programming and space it out. We also deleted unused imports and deleted any code that we didnt deem appropriate for the branch.

March 1, 2023

Today, I worked on writing a unit test for our application. It was a requirement for us, each, to write a unit test for each of our endpoint/responsibility. For me, this was a bit confusing as I felt like it was a bigger version of the assert 1=1 but on a bigger scale. The biggest aha moment for me was that we need to have a mock user, in order to 1) sign into our application to hit our protected endpoints and then 2) be able to have mock data that is also being returned to make sure we can correctly hit the endpoint. This was the dependency injection that was covered but I thought we would need to actually have data come back to prove it worked. I also completed the front end component for my responsibilities and we worked to deploying our application.

February 28, 2023

Today, I continued to work on the detail page to make sure it shows up within the list view. I had to manipulate the router and queries to make sure I could access the name of the swooper via the detail page. The “aha” moment for me was working with Daniel and understanding that although we’re working on similar features, his view was from the user looking at customer posts and my feature was the customer’s view looking at their own post. The customer would need to see who accepted their post and the respective contact information. This took me quite some time to understand how to access the correct data and I had to refer back to the table migrations to make sure that my SQL statements made sense.

February 27, 2023

Today I continued to work on completing the front-end for both my lists. I had never created a detail view in react before so this was quite challenging for me. Upon discussing it with my team, I was able to get help from them to help make the list view a react component as oppose to in Django where we would have an endpoint for a specific pickup id. Once I got the detail page embedded into the list view, I struggled with the page disappearing when you click on a post. My biggest “aha” moment was solidifying the

February 26, 2023

Today I continued to work on my react components. The biggest “aha” moment caused quite a bit of re-working. Since the detail page of our customer lists requires certain properties, I had to switch out the the base model we used from SwoopsOut to SwoopsOutWithUsers to include the information which I will need to access. A majority of the day was spent working on the back-end to make sure my endpoints were working correctly, as intended.

February 24, 2023

Today, I continued working on my front-end view of the list of customer posts. It was tough trying to remember how to write react after a few weeks off so instead of trying to remember it, I referred to the video lectures from Mod2 - specifically state management. I know there was a new aspect to consider, which was utilizing the authorization and token within our state to make sure the page is only showing up for the person that is logged in. Referring back to Django, this was like the @ loginrequired decorator on the views. The “aha” moment for me was trying to tie it to how we learned it via Django and what that looks like there and what that means in regards to FastAPI

February 21,2023

Today, we wanted to get an instructor to double check our progress over the weekend. Paul mentioned that if it works, that’s good enough. If we wanted to, we can also go through the FastAPI tutorial to get another approach but at least we have ours working. From there, we had Alex, the SEIR, come in and help explain to us what we were missing in regards to the token. He was able to show us his project and walk through his thought process which pointed us in the right direction. Today I worked on fixing my get_all_customerpost function to be more dynamic and was able to get help from Daniel in order to understand what changes need to be made to allow us to do that. I’ve also fixed my router to take in the correct parameter. I’ve started on my show detail of a customer post function but after taking a step back and reviewing the functionality through our excalidraw, I noticed that we may not need one afterall. My “aha” moment was actually referring back to our domain driven design / excalidraw to reference the functionality of what we’re building. In this event, our swooper and our customer need to access a detail page of the posting. This would look the same to a customer as it would to a swooper. We decided that we would just need another router to go through and using the same method within our Swoops Repo rather creating two of the same showing detail function. My second “aha” moment was making sure to reference the table migration to make sure my SQL statements reflected what was on the table, regardless, of what data I needed. By matching those up, my code finally worked.

February 20, 2023

Today, we met up to continue working on the authorization. We reviewed the code that was given to us for the Galvanize authentication library. We also revisited Curtis’ video lecture to try and understand where we may have missed a step. Upon looking back at the gitlab which contained the library, we were able to make better sense of what was happening under the hood. We also were able to take a step back and look at it from a conceptual point of view. My “aha” moment was when thinking about how a password gets stored. When a customer uses our interface, we will be storing the hashed password into our data table. So we have to make sure the password is hashed to be protected in the event someone has access to our table. As for the back-end, we have to have a way to de-crypt the password and check that value of the de-crypted password and check to make sure it equals the password the customer is entering. This allowed us to make sense of what variables we needed to plug in and how we wanted to name certain variables in our authentication functions.

February 16, 2023

Today, I worked on creating the get_all method of the SwoopsRepository as well as the get_all endpoint. I coordinated with Andy who created the table for the Swoops repository and we were able to work together to solidify which variables we needed. We ended up adding a variable called “status” which will help me show the status of all of a user’s swoop entry. We coordinated with Zeke who is getting the list of available swoops and made sure we understood how the customer_id and swooper_id interacted with eachother once someone accepts a swoop job. The “aha” moment today was the git workflow. We had missed a crucial step in our git workflow and that caused us an hour and a half reverting mess that we had to have multiple instructors help walk us through. We were able to clear up confusion and added a step to our workflow which made sure our local repositories were up to date in the event someone had merged into the main branch at the same time we were working on our branch.

February 15, 2023

Today, as a team, we mainly focused on organization. We focused on getting the docker compose yaml file up to make sure that everyone has a base level to start. Since we have to make sure everything is standard across the group, it was imperative that we made sure our containers and tables worked first, then we pushed it to the main branch and everyone pulled from there. We also went over the issues board and made sure we approved each team members’ respective issues. We also confirmed the customer journey which helped us solidify our models and the relationships between the models. The “aha” moment for me was to make sure we walked through our wireframe WITH our issue board to make sure we covered all bases. It was really important for us to understand the direction of our application before u

import React from 'react';
import ExpandingCards from './ExpandingCard';
import './MainPageLoggedIn.css';

function  ExpandingMain() {
const images = [
    {
        id: 1,
        title: "Need to get rid of some Trash?",
        url: 'https://i.imgur.com/RGB5La2.jpg',
        active: true,
        button: "Schedule a pickup",
        nav: "/newpickup"
    },
    {
        id: 2,
        title: "Want to Become a Swooper?",
        url: 'https://i.imgur.com/wfu42RC.jpg',
        active: false,
        button: "Swooper Form",
        nav: "/swoopers/signup"
    },
    {
        id: 3,
        title: "Already a Swooper?",
        url: 'https://i.imgur.com/tXJ4DL9.jpg',
        active: false,
        button: "Swoop List",
        nav: "/listings"
    },
    {
        id: 4,
        title: "Need to edit your Profile?",
        url: 'https://i.imgur.com/HDa8HL7.jpg',
        active: false,
        button: "Update Profile",
        nav:"/profile"
    },
    {
        id: 5,
        title: "Meet the Team",
        url: 'https://i.imgur.com/J4gitgN.jpg',
        active: false,
        button: "About us"
    },


]
return (
    <>
    <div className="pt-5 pb-5"></div>
    <h1 style={{textAlign: "center", margin:"-37px"}} className="pt-5">Explore</h1>
    <div className="expanding-body">

        <ExpandingCards data={images}/>
    </div>

    </>
)}

export default ExpandingMain

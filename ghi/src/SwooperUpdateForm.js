import React, { useState, useEffect} from 'react'
import { useAuthContext} from "./Auth";
import { useNavigate } from "react-router-dom";


function SwooperUpdateForm() {
  const {token} = useAuthContext();
  const [user, setUser] = useState([]);
  const [car, setCar] = useState('')
  const [license_number, setLicenseNumber] = useState('')

  const handleCarChange = (e) => {
    const value = e.target.value;
    setCar(value);
  }

////////////////////////////////////////////////////////
  const handleLicenseChange = (e) => {
    const value = e.target.value;
    setLicenseNumber(value);
  }
////////////////////////////////////////////////////////
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault()

    const url = `${process.env.REACT_APP_SWOOP_SERVICE_API_HOST}/api/accounts/${user.user_id}`;
    const fetchConfig = {
        method: "put",
        body: JSON.stringify({
            car: car,
            license_number: license_number,
        }),
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        console.log("Swooper sign-up successful!")
        console.log(token)
        navigate('/loggedin');
        return response.json()
    }
  }

    useEffect(() => {
      const fetchUserData = async () => {
        const URL = `${process.env.REACT_APP_SWOOP_SERVICE_API_HOST}/api/accounts`;

        const response = await fetch(URL, {
            headers: { Authorization: `Bearer ${token}` },
        });

        if (response.ok) {
            const data = await response.json();
            setUser(data)
        }
    }
        fetchUserData();
      }, [token]);


   return (
    <>
  <div className="row">
    <div className="col-md-6 order-md-2">
      <img
        src="https://img.freepik.com/free-vector/sign-up-concept-illustration_114360-7865.jpg?w=1380&t=st=1678405631~exp=1678406231~hmac=5bf8755ad3faeaa8b6a7ca7e7f08608ef51d2cdb51de15eb6bf54b170b915f9f"
        alt="siliconValleyCore"
        style={{
          width: '75%',
          marginTop: '10%'
        }}
      />
    </div>
    <div className="col-md-6 order-md-1 d-flex justify-content-center">
      <form
        onSubmit={handleSubmit}
        style={{
          width: '45%',
          marginTop: '10%'
        }}
      >
        <h1 className="text-center fw-bold fs-1 h3 mb-3 fw-normal">Join the Team!</h1>
        <div className="mb-3">
          <div className="text-center">
            <p>Please fill-in the information below to become a Swooper!</p>
          </div>
        </div>
        <div className="form-floating">
          <input type="text" className="form-control" value={car} onChange={handleCarChange} />
          <label>Car:</label>
        </div>
        <br />
        <div className="form-floating">
          <input type="text" className="form-control" value={license_number} onChange={handleLicenseChange} />
          <label>License Number:</label>
        </div>
        <br />
        <button className="w-100 btn btn-lg btn-success" type="submit">Sign Up!</button>
        <p className="text-center mt-5 mb-3 text-muted">© 2023 SWÜP Technologies Inc.</p>
      </form>
    </div>
  </div>
</>


  );
};

export default SwooperUpdateForm;
